#!/bin/sh
if [ "$TERM" = "linux" ]; then
  /bin/echo -e "
  \e]P03a3a3a
  \e]P1cc0066
  \e]P266cc00
  \e]P3cccc00
  \e]P40066cc
  \e]P5cc00cc
  \e]P600cccc
  \e]P7ebebeb
  \e]P84d4d4d
  \e]P9e57fb2
  \e]PAb2e57f
  \e]PBe5e57f
  \e]PC7fb2e5
  \e]PDe57fe5
  \e]PE7fe5e5
  \e]PFdedede
  "
  # get rid of artifacts
  clear
fi

